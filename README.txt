
Deber� generar los endpoints correspondientes con su respuesta en formato JSON para:

�	Obtener las habitaciones seg�n su estado (libre, ocupada, mantenimiento, limpieza) o tipo (est�ndar, normal, suite).

------------@RequestMapping(value="/habitacion")
@GetMapping(value="/findByEstado/{estado}")
@GetMapping(value="/findByTipo/{tipo}")



�	Reservar una habitaci�n.
----------@RequestMapping(value="/reservar/")
@PostMapping(value="/reservar")


�	Transicionar el estado de las habitaciones.
------------@RequestMapping(value="/habitacion")
@PostMapping(value="/transicionarHabitacion")



�	CRUD habitaciones.
------------@RequestMapping(value="/habitacion")
@GetMapping(value="/listar")
@GetMapping(value="/findByEstadolibre")
@PostMapping(value="/save")
@PostMapping(value="/update")
@PostMapping(value="/delete")
