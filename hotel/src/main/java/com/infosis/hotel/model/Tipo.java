package com.infosis.hotel.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipo")
public class Tipo  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idTipo")
	private Long idTipo;
	
	@Column(name="descripcion")
	private  String descripcion;
	
	@Column(name="maxOcupante")
	private  int maxOcupante;
	
	@Column(name="estadoLogico")
	private  int estadoLogico;

	public Long getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getMaxOcupante() {
		return maxOcupante;
	}

	public void setMaxOcupante(int maxOcupante) {
		this.maxOcupante = maxOcupante;
	}

	public int getEstadoLogico() {
		return estadoLogico;
	}

	public void setEstadoLogico(int estadoLogico) {
		this.estadoLogico = estadoLogico;
	}
	
}
