package com.infosis.hotel.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="estado")
public class Estado implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idEstado")
	private Long idEstado;
	

	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="idEstadoPadre")
	private String idEstadoPadre;
	
	@Column(name="estadoLogico")
	private int estadoLogico;

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdEstadoPadre() {
		return idEstadoPadre;
	}

	public void setIdEstadoPadre(String idEstadoPadre) {
		this.idEstadoPadre = idEstadoPadre;
	}

	public int getEstadoLogico() {
		return estadoLogico;
	}

	public void setEstadoLogico(int estadoLogico) {
		this.estadoLogico = estadoLogico;
	}
	

	
}
