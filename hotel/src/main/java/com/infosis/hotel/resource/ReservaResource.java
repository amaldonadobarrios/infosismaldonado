package com.infosis.hotel.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infosis.hotel.model.Habitacion;
import com.infosis.hotel.model.Reserva;
import com.infosis.hotel.repository.EstadoRepository;
import com.infosis.hotel.repository.HabitacionRepository;
import com.infosis.hotel.repository.ReservaRepository;

@RestController
@RequestMapping(value="/reservar/")
public class ReservaResource {

	@Autowired
	ReservaRepository reservaReposiory;
	@Autowired
	HabitacionRepository habitacionReposiory;
	@Autowired
	EstadoRepository estadoReposiory;
	
	@GetMapping(value="/listar")
	public List<Reserva> getAll(){
		return reservaReposiory.findAll();
		}
	
	
	@PostMapping(value="/ejecutar")
	public String persist(@RequestBody final Reserva reserva){
		//validar reserva 
		boolean valEstado=false;
		boolean valOcupantes=false;
		boolean valexistehabitacion=false;
		Habitacion hb=habitacionReposiory.findByIdHabitacion(reserva.getHabitacion().getIdHabitacion());
		String mensaje=null;
		if (hb!=null) {
			valexistehabitacion=true;
			//Estado de Habitacion =libre
			System.out.println(hb.getEstado().getDescripcion());
			if (hb.getEstado().getIdEstado()==Long.valueOf(1)) {
				valEstado=true;
			}
			if (reserva.getOcupantes()<=hb.getTipo().getMaxOcupante()) {
				valOcupantes=true;
			}	
		}
		if (valexistehabitacion) {
		if (valEstado) {
			if (valOcupantes) {
				try {
					Reserva  i=reservaReposiory.save(reserva);
					if (i.getIdReserva()>0) {
						hb.setEstado(estadoReposiory.findById(Long.valueOf(3)).get());
						habitacionReposiory.save(hb);
						mensaje="Se Registró la reserva correctamente";
					}	
				} catch (Exception e) {
					mensaje="Error al registrar la reserva";
				}
				
			}else {
				mensaje="Se Excedio el cupo maximo de personas";
			}
		}else {
			mensaje="Habitación no esta 'LIBRE'";
		}
		}else {
			mensaje="Habitación no existe";
		}
		return mensaje;
	}
}
