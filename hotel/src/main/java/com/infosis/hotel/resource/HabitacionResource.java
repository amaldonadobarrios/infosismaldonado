package com.infosis.hotel.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infosis.hotel.model.Estado;
import com.infosis.hotel.model.Habitacion;
import com.infosis.hotel.model.Usuario;
import com.infosis.hotel.repository.EstadoRepository;
import com.infosis.hotel.repository.HabitacionRepository;
import com.infosis.hotel.repository.UsuarioRepository;
import com.infosis.hotel.transfer.TransfTransicionEstado;

@RestController
@RequestMapping(value="/habitacion")
public class HabitacionResource {

	@Autowired
	HabitacionRepository habitacionReposiory;
	
	@Autowired
	EstadoRepository estadoReposiory;
	
	@Autowired
	UsuarioRepository UsuarioReposiory;
	
	
	
	//	Obtener las habitaciones según su 
	//estado (libre, ocupada, mantenimiento, limpieza) o
	//tipo (estándar, normal, suite).
	@GetMapping(value="/findByEstado/{estado}")
	public List<Habitacion> getAllxEstado(@PathVariable Long estado){
		return habitacionReposiory.findByEstado(estado);
	}
	@GetMapping(value="/findByTipo/{tipo}")
	public List<Habitacion> getAllxTipo(@PathVariable Long tipo){
		return habitacionReposiory.findByTipo(tipo);
	}
	
	@GetMapping(value="/findByEstadolibre")
	public List<Habitacion> getAllxEstado(){
		return habitacionReposiory.findByEstado(Long.valueOf(1));
	}

	@GetMapping(value="/listar")
	public List<Habitacion> getAll(){
		return habitacionReposiory.findAll();
		}
	
	@PostMapping(value = "/transicionarHabitacion")
	public String transicionar(@RequestBody TransfTransicionEstado trasferencia){
		
		boolean valexistehabitacion=false;
		boolean valtransicionposible=false;
		boolean rolvalido=false;
		boolean valexisteusuario=false;
		String mensaje=null;
		Optional<Usuario> adm= UsuarioReposiory.findById(trasferencia.getIdUsuario());
		if (adm!=null) {
			valexisteusuario=true;
			if(adm.get().getRol().getIdRol()==Long.valueOf(1)|| adm.get().getRol().getIdRol()==Long.valueOf(2) ) {
				rolvalido=true;
			}
		}
		//obtener la habitacion
		Habitacion hb=habitacionReposiory.findByIdHabitacion(trasferencia.getIdHabitacion());
		if (hb!=null) {
			valexistehabitacion=true;
			//obtener Transiciones posibles
			List<Estado> EstadosPosibles= estadoReposiory.findByEstadoPadre(String.valueOf(trasferencia.getIdEstado()));
			for (Estado estado : EstadosPosibles) {
				if (estado.getIdEstado()==hb.getEstado().getIdEstado()) {
					valtransicionposible=true;
				}
			}
		}
		
		//verificacion y cambio de estado
		if(valexisteusuario) {
			if(rolvalido) {
				if (valexistehabitacion) {
					if (valtransicionposible) {
						try {
							System.out.println(hb.getEstado().getDescripcion());
							
							hb.setEstado((estadoReposiory.findById(trasferencia.getIdEstado())).get());
							
							mensaje="Transición efectuada con éxito";
						} catch (Exception e) {
							mensaje="Ocurrio un Error  en la transcición";
						}
					}else {
						mensaje= "Transicion no es correcta con el flujo";
					}
					
				}else {
					mensaje="Habitación no existe";
				}
				
			}else {
				mensaje="Usuario no tiene privilegios";
			}
		}else {
			mensaje="Usuario no existe";
		}
		
		
		return mensaje;
	}
	
	
	
	
	
	
	@PostMapping(value="/save")
	public List<Habitacion> persist(@RequestBody final Habitacion habitacion){
		habitacionReposiory.save(habitacion);
		return habitacionReposiory.findAll();
	}
	
	@GetMapping(value="/findxnumero/{num}")
	public List<Habitacion> busarxnumero(@PathVariable int num){
		return habitacionReposiory.findBynumero(num);
	}
	
	@PutMapping(value="/update")
	public List<Habitacion> update(@RequestBody final Habitacion habitacion){
			habitacionReposiory.save(habitacion);
		return habitacionReposiory.findAll();
	}
	@PostMapping(value="/delete")
	public Habitacion delete(@RequestBody final Habitacion habitacion){
			Habitacion ha =habitacionReposiory.findByIdHabitacion(habitacion.getIdHabitacion());
			ha.setEstadoLogico(0);
			habitacionReposiory.save(ha);
		return habitacionReposiory.findByIdHabitacion(ha.getIdHabitacion());
	}
}
