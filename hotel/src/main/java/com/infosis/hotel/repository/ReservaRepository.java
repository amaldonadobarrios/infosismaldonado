package com.infosis.hotel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infosis.hotel.model.Reserva;

public interface ReservaRepository extends JpaRepository<Reserva, Long>{
	
}
