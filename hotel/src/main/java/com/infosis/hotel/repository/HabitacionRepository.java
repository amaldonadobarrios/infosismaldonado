package com.infosis.hotel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.infosis.hotel.model.Habitacion;

public interface HabitacionRepository  extends JpaRepository<Habitacion, Long>{
	 List<Habitacion> findBynumero(int Numero);
	 Habitacion findByIdHabitacion(Long idhabitacion);
	 @Query("select u from Habitacion u where u.estado.idEstado = :estado")
	 List<Habitacion> findByEstado(@Param("estado") Long estado);
	 @Query("select u from Habitacion u where u.tipo.idTipo = :tipo")
	 List<Habitacion> findByTipo(@Param("tipo") Long tipo);
	 
}
