package com.infosis.hotel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.infosis.hotel.model.Estado;


public interface EstadoRepository extends JpaRepository<Estado,Long>{
	 @Query("select u from Estado u where u.idEstadoPadre like %?1%")
	  List<Estado> findByEstadoPadre(String estado);

}
