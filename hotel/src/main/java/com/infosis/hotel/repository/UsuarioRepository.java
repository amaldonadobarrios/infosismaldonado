package com.infosis.hotel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infosis.hotel.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

}
