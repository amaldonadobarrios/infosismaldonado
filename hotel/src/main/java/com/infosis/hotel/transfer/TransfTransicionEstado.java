package com.infosis.hotel.transfer;

import java.io.Serializable;

public class TransfTransicionEstado implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private Long  idUsuario;
private Long idHabitacion;
private Long idEstado;
public Long getIdUsuario() {
	return idUsuario;
}
public void setIdUsuario(Long idUsuario) {
	this.idUsuario = idUsuario;
}
public Long getIdHabitacion() {
	return idHabitacion;
}
public void setIdHabitacion(Long idHabitacion) {
	this.idHabitacion = idHabitacion;
}
public Long getIdEstado() {
	return idEstado;
}
public void setIdEstado(Long idEstado) {
	this.idEstado = idEstado;
}

}
