package com.infosis.hotel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.infosis.hotel.repository")
@SpringBootApplication
public class HotelApplication {


	
	public static void main(String[] args) {
		SpringApplication.run(HotelApplication.class, args);
		
	
	}

}
